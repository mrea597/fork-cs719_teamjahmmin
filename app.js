// Setup code
// ------------------------------------------------------------------------------
var express = require('express');
var app = express();
app.set('port', process.env.PORT || 3000);

var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({
    defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: false
}));

// required the filesystem module @martin
var fs = require('fs');
// require jimp for generating thumbnails @martin
var jimp = require("jimp");

// require formidable for handling audio/video uploads
var formidable = require("formidable");

// use express-session to create in-memory sessions @martin
var session = require('express-session');
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: "teamJAHM"
}));

// require the JavaScript to process image uploads from Froala @jingyuan
var upload_image = require("./image_upload.js");
var FroalaEditor = require('./node_modules/wysiwyg-editor-node-sdk/lib/froalaEditor.js');

// allow us to use SQLite3 from node.js & connect to a database @martin
var sqlite3 = require('sqlite3').verbose();
//returns a new Database object, and opens the SQLite database 'blod-db.db‘ automatically
var db = new sqlite3.Database('blog-db.db');

//import external DAO module which contains database statements
var dao = require('./dao.js');

// Code for authentication starts from here @martin
// ---------------------------------------------------------------------------------

// require the 'passport' module for authentication
var passport = require('passport');
// use the local authentication strategy
var LocalStrategy = require('passport-local').Strategy;

// define the local authentication strategy
var localStrategy = new LocalStrategy(function (username, password, done) {

    // // query the blog database for the supplied username and retrieve all columns
    // db.all("SELECT * FROM Users WHERE username = ?", [username], function (err, rows) {

    //     // if the query returns no rows, then the username was not found in the database
    //     if (rows.length == 0) {
    //         return done(null, false, { message: 'Invalid username!' });
    //     }

    //     // there should only be 1 row returned by SQL query if the username was found
    //     var user = rows[0];

    //     // retrieve the password string from the 'rows' object returned by the query
    //     var userPasswordInDb = rows[0].password;

    //     
    //     if (userPasswordInDb !== password) {
    //         return done(null, false, { message: 'Invalid password!' });
    //     };

    //     // if the above validation has passed, then user is authenticated
    //     done(null, user);
    // });   

    // query the blog database for the supplied username and retrieve all columns
    dao.getUser(username, function (user) {
        // there should only be 1 row returned by SQL query if the username was found
        if (!user) {
            return done(null, false, {
                message: 'Invalid user'
            });
        };

        // if the provided password, does not match what is in the database
        if (user.password !== password) {
            return done(null, false, {
                message: 'Invalid password'
            });
        };

        // if the above validation has passed, then user is authenticated
        done(null, user);
    });

});

// method to be called to save the currently logged in username to the session
passport.serializeUser(function (user, done) {
    done(null, user.username);
});

// method to be called to retrieve all data in the database related to the provided username
passport.deserializeUser(function (username, done) {

    // // query the blog database for the supplied username
    // db.all("SELECT * FROM Users WHERE username = ?", [username], function (err, rows) {  
    //     if (rows.length > 0) {
    //         user = rows[0];
    //         done(null, user);
    //     }
    // });

    // query the blog database for the supplied username
    dao.getUser(username, function (user) {
        done(null, user);
    });

});

// passport should use the 'local strategy' for authentication
passport.use('local', localStrategy);

// initialise passport
app.use(passport.initialize());
// request passport to use sessions to store its data
app.use(passport.session());

// ---------------------------------------------------------------------------------
// code for authentication ends @martin


// check if user is logged in
function isLoggedIn(req, res, next) {
    // if user is authenticated, execute the next function 
    if (req.isAuthenticated()) {
        return next();
    }

    // redirect them to the login page
    res.redirect("/login");
}

//--------------------- ROUTE HANDLERS -------------------------------------------

//-----Routes for displaying and editing ARTICLES-------------------------------
app.get(['/', '/home', '/article'], function (req, res) {

    var username = null;
    var avatar = null;

    if (req.isAuthenticated()) {
        username = req.user.username;
    }

    dao.getUser(username, function (user) {
        dao.getAllArticles(function (articles) {

            if (user != null) {
                avatar = user.avatar;
            }

            var sidebarLinks = [];
            for (var i = 0; i < 5; i++) {
                sidebarLinks[sidebarLinks.length] = articles[i];
            }

            var data = {
                username: username,
                avatar: avatar,
                articles: articles,
                sidebarLinks: sidebarLinks,
                loggedOut: req.query.loggedOut,
                newArticleCreated: req.query.newArticleCreated,
                accountUpdated: req.query.accountUpdated,
                articleDeleted: req.query.articleDeleted,
                userDeleted: req.query.userDeleted,
                allArticles: true
            }

            res.render('home', data);
        });
    });

});

app.get('/addPost', isLoggedIn, function (req, res) {

    var username = req.user.username;
    var avatar = null;

    dao.getUser(username, function (user) {

        avatar = user.avatar;

        var data = {
            username: username,
            avatar: avatar
        }

        res.render('addPost', data);
    });


});

app.post('/addPost', function (req, res) {

    // create a new formidable form object
    var form = new formidable.IncomingForm();

    // check if any new images require thumbnails
    checkForImages();

    // when file upload detected, upload file to the multimedia folder
    form.on("fileBegin", function (name, file) {
        // console.log(req.user.username);
        // console.log(articleId);
        file.path = __dirname + "/public/uploads/multimedia/" + req.user.username + "-" + file.name;
    });

    // parse the submitted form data using formidable
    form.parse(req, function (err, fields, files) {

        // check the file type of the upload file
        var fileType = files.fileUpload.type;
        var audioFile = null;
        var videoFile = null;

        // if file type starts with '/audio'
        if (fileType.startsWith('audio')) {
            audioFile = req.user.username + "-" + files.fileUpload.name;
            // else if file type starts with '/video'
        } else if (fileType.startsWith('video')) {
            videoFile = req.user.username + "-" + files.fileUpload.name;
        }

        // LINK to be removed when testing complete!
        var articleDetails = {
            username: req.user.username,
            title: fields.title,
            content: fields.content,
            audio: audioFile,
            video: videoFile,
            link: 'no_link'
        }

        dao.createArticle(articleDetails, function (articleId) {

            res.redirect("/home?newArticleCreated=true");
        })
    });

});

app.get('/article/:id', function (req, res) {

    var isArticleAuthor = false;

    var username = null;
    var avatar = null;

    if (req.isAuthenticated()) {
        username = req.user.username;
    }

    dao.getUser(username, function (user) {
        dao.getArticle(req.params.id, function (article, comments) {

            if (user != null) {
                avatar = user.avatar;
            }

            if (article.author == username) {
                isArticleAuthor = true;
                //also allow Article Author to delete comments on the article
                for (var j = 0; j < comments.length; j++) {
                    comments[j].isArticleAuthor = true;
                }
            }

            // add isCommentAuthor flag which enables display of edit and delete button on own comments
            for (var i = 0; i < comments.length; i++) {
                if (comments[i].author == username) {
                    comments[i].isCommentAuthor = true;
                }
            }

            var data = {
                username: username,
                avatar: avatar,
                articles: article,
                allArticles: false,
                isArticleAuthor: isArticleAuthor,
                comments: comments,
                commentDeleted: req.query.commentDeleted
            }

            res.render('articleView', data);
        });
    });
});

app.post('/editPost', function (req, res) {

    checkForImages();

    var articleId = req.body.articleId;

    dao.getArticle(articleId, function (article) {
        var username = null;
        if (req.isAuthenticated()) {
            username = req.user.username;
        }

        var data = {
            layout: 'no-nav',
            username: username,
            articles: article,
            allArticles: false,
        }

        res.render('editPost', data);
    });
})

app.post('/saveEditedPost', function (req, res) {
    var articleId = req.body.articleId;
    var newTitle = req.body.title;
    var newContent = req.body.content;

    dao.updateArticle(articleId, newTitle, newContent, function () {
        res.redirect("/article/" + articleId);
    })

});

app.post('/deletePost', function (req, res) {

    var articleId = req.body.articleId;
    var fromAccountPage = req.body.fromAccountPage;

    dao.deleteArticle(articleId, function () {
        if (fromAccountPage) {
            res.redirect("/accountAndPosts?articleDeleted=true")
        } else {
            res.redirect("/home?articleDeleted=true");
        }
    })

})

//------------------------------------------------------------------------------

//-----Routes for displaying and editing COMMENTS-------------------------------

app.post('/addComment', function (req, res) {

    var username = req.body.username;
    var articleId = req.body.articleId;
    var newComment = req.body.newComment;

    dao.addComment(username, articleId, newComment, function () {
        res.redirect("/article/" + articleId);
    });
});

app.post('/editComment', function (req, res) {

    var commentId = req.body.commentId;

    dao.getComment(commentId, function (comment) {
        var username = null;
        if (req.isAuthenticated()) {
            username = req.user.username;
        }

        var data = {
            layout: 'no-nav',
            username: username,
            comment: comment
        }

        res.render('editComment', data);
    });
})

app.post('/saveEditedComment', function (req, res) {

    var commentId = req.body.commentId;
    var articleId = req.body.articleId;
    var newComment = req.body.content;

    dao.updateComment(commentId, newComment, function () {
        res.redirect("/article/" + articleId);

    })

});

app.post('/deleteComment', function (req, res) {

    var commentId = req.body.commentId;
    var articleId = req.body.articleId;
    var fromAccountPage = req.body.fromAccountPage;

    dao.deleteComment(commentId, function () {
        if (fromAccountPage) {
            res.redirect("/accountAndPosts?commentDeleted=true")
        } else {
            res.redirect("/article/" + articleId + "?commentDeleted=true");
        }

    });

});

//------------------------------------------------------------------------------

//-----Routes for USER Activities-----------------------------------------------

app.get('/signup', function (req, res) {

    // generate an array of filenames in the default-avatars folder
    var defaultAvatars = fs.readdirSync(__dirname + '/public/default-avatars');
    defaultAvatars.shift();

    app.post('/signup', function (req, res) {
        if (req.body.password != req.body.passwordCheck) {
            req.session.partialUserData = {
                fname: req.body.fname,
                lname: req.body.lname,
                dob: req.body.dob,
                country: req.body.country,
                username: req.body.username,
                description: req.body.description
            }
            res.redirect('/signup?passwordFail=true');
        }
        else {

            dao.createUser(req.body, function (err, user) {
                if (err) {
                    req.session.partialUserData = {
                        fname: req.body.fname,
                        lname: req.body.lname,
                        dob: req.body.dob,
                        country: req.body.country,
                        username: req.body.username,
                        description: req.body.description
                    }
                    res.redirect('/signup?usernameExists=true');
                }
                else {
                    delete req.session.partialUserData;

                    passport.authenticate('local')(req, res, function () {
                        res.redirect('/home');
                    })
                }

            });
        }
    });
});

//ajax call to get all usernames to be compared with user input
app.get("/getAllUsernames", function (req, res) {
    dao.getAllUsernames(function (usernames) {
        var usernamesString = JSON.stringify(usernames);
        res.status(200);
        res.type("text/plain");
        res.end(JSON.stringify(usernames));
    });
});

app.get('/login', function (req, res) {
    if (req.isAuthenticated()) {
        res.redirect("/home");
    } else {
        var data = {
            layout: 'no-nav',
            loginFail: req.query.loginFail,
            newAccountCreated: req.query.newAccountCreated
        }
        res.render('login', data);
    }
});


app.post('/login', passport.authenticate('local', {
    successRedirect: '/home',
    failureRedirect: '/login?loginFail=true'
}));

app.get('/logout', function (req, res) {
    req.logout();
    res.redirect("/home?loggedOut=true");
});

app.get('/accountAndPosts', isLoggedIn, function (req, res) {

    dao.getUser(req.user.username, function (user) {

        dao.getUserArticlesAndComments(user.username, function (articles, comments) {
            var data = {
                username: user.username,
                avatar: user.avatar,
                userData: user,
                articles: articles,
                comments: comments,
                commentDeleted: req.query.commentDeleted,
                articleDeleted: req.query.articleDeleted
            }
            res.render('accountAndPosts', data);

        });
    });

});

app.post('/account', function (req, res) {

    var defaultAvatars = fs.readdirSync(__dirname + '/public/default-avatars');
    defaultAvatars.shift();
    // if (req.session.partialUserData) {
    //     var data = {
    //         layout: 'no-nav',
    //         passwordFail: req.query.passwordFail,
    //         defaultAvatars: defaultAvatars,
    //         userData: req.session.partialEditUserData
    //     }
    //     res.render('account', data);
    // }
    // else {
    dao.getUser(req.user.username, function (user) {
        var data = {
            layout: 'no-nav',
            passwordFail: req.query.passwordFail,
            defaultAvatars: defaultAvatars,
            userData: user
        }
        res.render('account', data);
    });

    // }
});

app.post('/accountUpdate', function (req, res) {

    var defaultAvatars = fs.readdirSync(__dirname + '/public/default-avatars');
    defaultAvatars.shift();
    if (req.body.password != req.body.passwordCheck) {
        req.session.partialEditUserData = {
            fname: req.body.fname,
            lname: req.body.lname,
            dob: req.body.dob,
            country: req.body.country,
            username: req.body.username,
            description: req.body.description
        }

        var data = {
            layout: 'no-nav',
            passwordFail: true,
            defaultAvatars: defaultAvatars,
            userData: req.session.partialEditUserData
        }
        res.render('account', data);

    } else {

        dao.updateUser(req.body, function () {
            delete req.session.partialEditUserData;
            res.redirect('/home?accountUpdated=true');
        });
    }
});

app.post('/deleteUser', function (req, res) {

    dao.deleteUser(req.user.username, function () {
        req.logout();
        res.redirect("/home?userDeleted=true");
    });

});

// this route handles the processing of all images and creates an array of all media files
// which are passed to the gallery view @martin
app.get('/gallery', function(req, res) {

    dao.getAllArticles(function (articles) {

        var username = null;
        if (req.isAuthenticated()) {
            username = req.user.username;
        }
        
        // this variable stores an array of media from only the currently logged in user
        var currentUsersMedia = [];
        // this variable stores an array of media from ALL users 
        var allUsersMedia = [];

        for (var i = 0; i < articles.length; i++) {

            var content = articles[i].content;
            var articleId = articles[i].articleId;
            var author = articles[i].author;

            // regex to scan the article content for images uploaded to our server
            var imageRegex = new RegExp('\\/uploads\\/images\\/[0-9a-z]+(.png|.jpg|.jpeg|.gif|.svg)', 'g');
            // check content for any matches with the regex and store in an array
            var editorImages = content.match(imageRegex);

            // if the regex found only one image in the article content
            if (editorImages != null && editorImages.length == 1) {
                // remove the path in-front of the file name
                var justTheFilename = editorImages[0].replace('/uploads/images/', '');
                // handle gif files separately as they do not have resized thumbnails
                if (justTheFilename.indexOf('.gif') != -1) {
                    allUsersMedia.push({ id: articleId, gifFilename: justTheFilename});
                    if (username == author) {
                        currentUsersMedia.push({ id: articleId, gifFilename: justTheFilename});
                    }
                } else {
                    allUsersMedia.push({ id: articleId, imageFilename: justTheFilename});
                    if (username == author) {
                        currentUsersMedia.push({ id: articleId, imageFilename: justTheFilename});
                    }
                }
            // else if the regex found more than one match in the article content
            } else if (editorImages != null && editorImages.length > 1) {

                for (var j = 0; j < editorImages.length; j++) {
                    // remove the path in-front of the file name
                    var justTheFilename = editorImages[j].replace('/uploads/images/', '');
                    // handle gif files separately as they do not have resized thumbnails
                    if (justTheFilename.indexOf('.gif') != -1) {
                        allUsersMedia.push({ id: articleId, gifFilename: justTheFilename});
                        if (username == author) {
                            currentUsersMedia.push({ id: articleId, gifFilename: justTheFilename});
                        }
                    } else {
                        allUsersMedia.push({ id: articleId, imageFilename: justTheFilename});
                        if (username == author) {
                            currentUsersMedia.push({ id: articleId, imageFilename: justTheFilename});
                        }
                    }
                }
            }

            // regex to scan the article for embedded youtube links
            var youtubeRegex = new RegExp('youtube.com\\/embed\\/.+?(?=\\?)', 'g');
            // check content for any matches with the regex and store in an array
            var editorYoutube = content.match(youtubeRegex);

            // if the regex found only one image in the article content
            if (editorYoutube != null && editorYoutube.length == 1) {
                // remove the path in-front of the youtube id#
                var justTheYouTubeId = editorYoutube[0].replace('youtube.com/embed/', '');

                allUsersMedia.push({id: articleId, youtubeId: justTheYouTubeId});
                if (username == author) {
                    currentUsersMedia.push({id: articleId, youtubeId: justTheYouTubeId});
                }
            // else if the regex found more than one match in the article content    
            } else if (editorYoutube != null && editorYoutube.length > 1) {
                for (var j = 0; j < editorYoutube.length; j++) {
                    // remove the path in-front of the youtube id#
                    var justTheYouTubeId = editorYoutube[j].replace('youtube.com/embed/', '');
                    allUsersMedia.push({id: articleId, youtubeId: justTheYouTubeId});
                    if (username == author) {
                        currentUsersMedia.push({id: articleId, youtubeId: justTheYouTubeId});
                    }
                }
            }

            // video/audio uploads are stored in the database
            var videoUpload = articles[i].video;
            var audioUpload = articles[i].audio;

            // if there is a video filename in the database
            if (videoUpload != null) {
                allUsersMedia.push({id: articleId, videoFilename: videoUpload});
                if (username == author) {
                    currentUsersMedia.push({id: articleId, videoFilename: videoUpload});
                }
            // else if there is a audio filename in the database    
            } else if (audioUpload != null) {
                allUsersMedia.push({id: articleId, audioFilename: audioUpload});
                if (username == author) {
                    currentUsersMedia.push({id: articleId, audioFilename: audioUpload});
                }
            }
        }

        var gallery = allUsersMedia;
        var isUserView = false;

        if (req.query.show == 'usergallery' && username != null) {
            gallery = currentUsersMedia;
            isUserView = true;
        }

        var data = {
            username: username,
            gallery: gallery,
            isUserView: isUserView,
        }

        res.render('gallery', data);      

    });
});

// function to check if there are new images uploaded on the server that require thumbnails
function checkForImages() {
    
    var fullsizeFolder = __dirname + "/public/uploads/images/";
    var thumbFolder = __dirname + "/public/uploads/thumbnails/";

    // generate an array of all files in the images folder
    var existingFullsize = fs.readdirSync(fullsizeFolder);
    // generate an array of all files in the thumbnails folder
    var existingThumbs = fs.readdirSync(thumbFolder);

    for (var i = 0; i < existingFullsize.length; i++){
        
        var fullsizeImg = existingFullsize[i];

        // if the image filename already exists in thumbnail folder
        if(!(existingThumbs.includes(fullsizeImg))) {

            if (fullsizeImg.endsWith(".png") || fullsizeImg.endsWith(".bmp") ||
            fullsizeImg.endsWith(".jpg") || fullsizeImg.endsWith(".jpeg")) {

                var inputFileName = fullsizeFolder + fullsizeImg;
                var outputFileName = thumbFolder + fullsizeImg;
                console.log("resizing a file...")
                generateThumbs(inputFileName, outputFileName);

            }
        }
    }
}

// function to generate thumbnail images
function generateThumbs(inputFilePath, outputFilePath) {

    var thumbWidth = 200;
    var thumbHeight = 200;
    
    // read the fullsize image folder
    jimp.read(inputFilePath, function (err, image) {

        // when jimp loads image, generate thumb & save to thumbnails folder
        image
            .scaleToFit(thumbWidth, thumbHeight)
            .write(outputFilePath, function (err) {
                if (err) {
                    console.error("Error saving file!");
                } 
            });

    })
}

//-----Routes for handling Froala file uploads---------------------------------

// route handler for saving images from Froala editor:
app.post("/image_upload", function (req, res) {

    upload_image(req, function (err, data) {

        if (err) {
            return res.status(404).end(JSON.stringify(err));
        }
        res.send(data);
    });
});
//Listen to the delete image request.
app.post('/delete_image', function (req, res) {

    // Do delete.
    FroalaEditor.Image.delete(req.body.src, function (err) {

        if (err) {
            return res.status(404).end(JSON.stringify(err));
        }

        return res.end();
    });
});

//------------------------------------------------------------------------------



// Serve files form "/public" folder
app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/"));

// --------------------------------------------------------------------------

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});